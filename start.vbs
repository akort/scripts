Set objExcel = CreateObject("Excel.Application")

'view the excel program and file, set to false to hide the whole process
	objExcel.Visible = False

'open an excel file (make sure to change the location) .xls for 2003 or earlier
'When running for the first time, Excel workbook must have a date in the (A,1) cell eg. (9.7.2018)

	Set objWorkbook = objExcel.Workbooks.Open("C:\Users\XXX\Desktop\testStart.xlsx")



' Adds -5 minutes to current time
clockIn = DateAdd("n",-5,Now())

' find the first empty cell in the first("A") column

For Each cell in objExcel.Columns(1).Cells
	If IsEmpty(cell) = True Then 

		' Check if there is data for today already
		If DateDiff("d",Now(),cell.Offset(-1,0)) = 0 Then
			:Exit For ' There is data, exit the loop
		Else 
		
		cell.Value = FormatDateTime(Now(),2) ' Date in mm/dd/yy
		cell.Offset(0,1).Value = FormatDateTime(clockIn,4) ' Write time to next cell
		
		End If

		: Exit For
	End If
Next





'save the existing excel file. use SaveAs to save it as something else
	objWorkbook.Save

'close the workbook
	objWorkbook.Close 

'exit the excel program
	objExcel.Quit

'release objects
	Set objExcel = Nothing
	Set objWorkbook = Nothing