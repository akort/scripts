Set objExcel = CreateObject("Excel.Application")

'view the excel program and file, set to false to hide the whole process
	objExcel.Visible = False

'open an excel file (make sure to change the location) .xls for 2003 or earlier
'Excel workbook must have a date in the (A,1) cell eg. (9.7.2018)

	Set objWorkbook = objExcel.Workbooks.Open("C:\Users\XXX\Desktop\testStart.xlsx")



' Adds 5 minutes to current time
clockOut = DateAdd("n",5,Now())

' find the first empty cell in the first("A") column

For Each cell in objExcel.Columns(1).Cells
	If IsEmpty(cell) = True Then 

		cell.Offset(-1,2).Value = FormatDateTime(clockOut,4) ' The cell above the empty and 2 cells to the right

		secs = DateDiff("s",cell.Offset(-1,1).Value,cell.Offset(-1,2).Value)
		workingHours = TimeSerial(0,0,secs)
		cell.Offset(-1,3).Value = FormatDateTime(workingHours,4)
		: Exit For
	End If
Next





'save the existing excel file. use SaveAs to save it as something else
	objWorkbook.Save

'close the workbook
	objWorkbook.Close 

'exit the excel program
	objExcel.Quit

'release objects
	Set objExcel = Nothing
	Set objWorkbook = Nothing